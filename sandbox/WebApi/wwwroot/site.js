﻿const uri = 'api/user';
let users = null;

$(document).ready(function () {
	getData();
});

function getCount(data) {
	const element = $('#counter');

	if (data) {
		element.text(data + ' User(s)');
	}
	else {
		element.html('No Users');
	}

}

function getData() {
	$.ajax({
		type: 'GET',
		url: uri,
		success: function (data) {
			const element = $('#users');

			element.empty();
			getCount(data.length);
			$.each(data, function (key, user) { $(
				'<tr>' +
					'<td>' + user.id + '</td>' +
					'<td>' + user.name + '</td>' +
					'<td>' + user.password + '</td>' +
					'<td><button onclick="editUser(' + user.id + ')">Edit</button></td>' +
					'<td><button onclick="deleteUser(' + user.id + ')">Delete</button></td>' +
				'</tr > '
				).appendTo(element);
			});

			users = data;
		}
	});
}

function closeInput() {
	$('#editUserDiv').css({ 'display': 'none' });
}

function createUser() {
	const nameElement = $('#add_name');
	const passwordElement = $('#add_password');

	const user = {
		'name': nameElement.val(),
		'password': passwordElement.val()
	};

	$.ajax({
		type: 'POST',
		url: uri,
		contentType: 'application/json',
		data: JSON.stringify(user),
		error: function (xhr, status, error) {
			alert('Error: ' + status);
		},
		success: function (result) {
			getData();

			nameElement.val('');
			passwordElement.val('');
		}
	});
}

function deleteUser(id) {
	$.ajax({
		type: 'DELETE',
		url: uri + '/' + id,
		success: function (result) {
			getData();
		}
	});
}

function editUser(id) {
	$.each(users, function (key, user) {
		if (user.id === id) {
			$('#edit_id').val(user.id);
			$('#edit_name').val(user.name);
			$('#edit_password').val(user.password);
		}
	});

	$('#editUserDiv').css({ 'display': 'block' });
}

$('.createUserForm').on('submit', function () {
	createUser();
});

$('.editUserForm').on('submit', function () {
	const idElement = $('#edit_id');
	const nameElement = $('#edit_name');
	const passwordElement = $('#edit_password');

	const user = {
		id: idElement.val(),
		name: nameElement.val(),
		password: passwordElement.val()
	};

	$.ajax({
		url: uri + '/' + idElement.val(),
		type: 'PUT',
		accepts: 'application/json',
		contentType: 'application/json',
		data: JSON.stringify(user),
		succes: function (result) {
			getData();
		}
	});

	closeInput();
});