﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Models;

namespace WebApi
{
	public class Startup
	{
		#region LifeCycle
// TODO: used?
		public Startup( IConfiguration configuration )
		{
			Configuration = configuration;
		}

		#endregion LifeCycle


		#region Operations

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure( IApplicationBuilder app, IHostingEnvironment env )
		{
			if ( env.IsDevelopment() )
			{
				app.UseDeveloperExceptionPage();
			}
			
			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseMvc();
		}


		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices( IServiceCollection services )
		{
			services.AddMvc();
		}

		#endregion Operations


		#region Properties
// TODO: used?
		public IConfiguration Configuration { get; }

		#endregion Properties
	}
}
