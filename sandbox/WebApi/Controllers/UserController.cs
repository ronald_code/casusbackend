﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using WebApi.Models;

namespace WebApi.Controllers
{
	[Produces("application/json")]
	[Route( "api/[controller]" )]
	public class UserController : Controller
	{
		#region LifeCycle

		static UserController()
		{
			// Controller continuously creates new instances: Generate data only once (static .ctor)
			m_Context = new List< User > {
				new User() { Id = 1, Name = "Admin", Password = "12345" },
				new User() { Id = 2, Name = "John", Password = "Doe" },
				new User() { Id = 3, Name = "Alice", Password = "Cooper" } };
		}

		#endregion LifeCycle


		#region Operations

		#region Delete

		[HttpDelete( "{id}" )]
		public IActionResult Delete( long id )
		{
			var user = m_Context.Where( u => u.Id == id ).FirstOrDefault();

			if ( user == null )
			{
				return NotFound();
			}

			m_Context.Remove( user );

			return NoContent();
		}

		#endregion Delete


		#region Get

		[HttpGet]
		public List< User > GetAll()
		{
			return m_Context;
		}


		[HttpGet( "{name}", Name = "GetUser" )]
		public IActionResult GetByName( string name )
		{
			var user = m_Context.Where( u => u.Name == name ).FirstOrDefault();

			if ( user == null )
			{
				return NotFound();
			}

			return new ObjectResult( user );
		}

		#endregion Get


		#region Post

		[HttpPost]
		public IActionResult Create( [FromBody] User user )
		{
			user.Id = m_Context.Any() ? m_Context.Max( u => u.Id ) + 1 : 1;

			m_Context.Add( user );
			
			return CreatedAtRoute( "GetUser", new { name = user.Name }, user );
			//return CreatedAtAction( "GetUser", "user", new { name = user.Name }, user );
		}

		#endregion Post


		#region Put

		[HttpPut( "{id}" )]
		public IActionResult Update( long id, [FromBody] User user )
		{
			var currentUser = m_Context.Where( u => u.Id == id ).FirstOrDefault();

			if ( currentUser == null )
			{
				return NotFound();	
			}

			currentUser.Name = user.Name;
			currentUser.Password = user.Password;

			return NoContent();
		}

		#endregion Put

		#endregion Operations


		#region Members

		private static readonly List< User > m_Context;

		#endregion Members
	}
}