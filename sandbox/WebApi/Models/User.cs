﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models
{
	public class User
	{
		#region Properties

		public long Id { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }

		#endregion Properties
	}
}
