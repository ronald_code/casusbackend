﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using Shared;

namespace ClientApp
{
	public class Program
	{
		public static void Main( string[] args )
		{
			MainAsync().GetAwaiter().GetResult();
		}

		public static async Task MainAsync()
		{
			// Discover endpoints from metadata.
			var discoveryResponse = await DiscoveryClient.GetAsync( Constants.IdentityServerUrl );

			if ( discoveryResponse.IsError )
			{
				Console.WriteLine( discoveryResponse.Error );
				return;
			}

			// Request access token.
			var tokenClient = new TokenClient( discoveryResponse.TokenEndpoint, Constants.ClientId, Constants.ClientSecret );
			//var tokenResponse = await tokenClient.RequestClientCredentialsAsync( Constants.ApiName );
			var tokenResponse = await tokenClient.RequestResourceOwnerPasswordAsync( "Alice", "Cooper", Constants.ApiName );

			if ( tokenResponse.IsError )
			{
				Console.WriteLine( tokenResponse.IsError );
				return;
			}

			Console.WriteLine( tokenResponse.Json );

			// Call API.
			var httpClient = new HttpClient();
			
			httpClient.SetBearerToken( tokenResponse.AccessToken );

			var httpResponse = await httpClient.GetAsync( Constants.ApiUrl + "/identity" );

			if ( !httpResponse.IsSuccessStatusCode )
			{
				Console.WriteLine( httpResponse.StatusCode );
			}
			else
			{
				var httpContent = await httpResponse.Content.ReadAsStringAsync();

				Console.WriteLine( JArray.Parse( httpContent ) );
			}
		}
	}
}
