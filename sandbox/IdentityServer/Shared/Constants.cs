﻿namespace Shared
{
	public static class Constants
	{
		public static readonly string IdentityServerUrl = "http://localhost:5000";
		public static readonly string ApiUrl = "http://localhost:5001";

		public static readonly string ApiName = "api1";
		public static readonly string ApiDescription = "My Web API";
		
		public static readonly string ClientId = "client";
		public static readonly string ClientSecret = "secret";

		public static readonly int AccessTokenLifetimeSeconds = 300;
	}
}
