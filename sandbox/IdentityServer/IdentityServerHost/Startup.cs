﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServerHost
{
	public class Startup
	{
		#region Operations

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			// TODO: AddDeveloperSigningCredential creates temporary key material for signing tokens.
			// http://docs.identityserver.io/en/release/topics/crypto.html#refcrypto
			services.AddIdentityServer()
				.AddDeveloperSigningCredential()
				.AddInMemoryApiResources( Config.GetApiResources() )
				.AddInMemoryClients( Config.GetClients() )
				.AddTestUsers( Config.GetUsers() );
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseIdentityServer();

			app.Run(async (context) =>
			{
				await context.Response.WriteAsync("Hello World!");
			});
		}

		#endregion Operations
	}
}
