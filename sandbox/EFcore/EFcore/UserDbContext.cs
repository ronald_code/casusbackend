﻿using Microsoft.EntityFrameworkCore;

namespace EFcore
{
	public class UserDbContext : DbContext
	{
		#region Operations

		protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
		{
			optionsBuilder.UseSqlite( "DataSource=users.db;" );
		}
		
		#endregion Operations


		#region Properties

		public DbSet< User > Users { get; set; }

		#endregion Properties
	}
}
