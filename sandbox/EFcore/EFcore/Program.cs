﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFcore
{
	class Program
	{
		static void Main( string[ ] args )
		{
			// Create context and ensure the DB already exists or is created.
			var context = new UserDbContext();
			
			context.Database.EnsureCreated();

			// Populate DB, if necessary
			if ( !context.Users.Any() )
			{
				context.Users.AddRange(
					new User() { Id = 1, Name = "Admin", Password = "12345" },
					new User() { Id = 2, Name = "John", Password = "Doe" },
					new User() { Id = 3, Name = "Alice", Password = "Cooper" } );
			}

			context.SaveChanges();

			// Read back all users
			foreach ( var user in context.Users )
			{
				Console.WriteLine( string.Format( "{0}\t|{1}\t|{2}", user.Id, user.Name, user.Password ) );
			}

			Console.ReadKey();

		}
	}
}
