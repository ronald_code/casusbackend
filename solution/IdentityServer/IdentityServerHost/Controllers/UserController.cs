﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServerHost
{
	[Produces("application/json")]
	[Route( "api/[controller]" )]
	public class UserController : Controller
	{
		#region LifeCycle

		public UserController( UserDbContext context )
		{
			m_Context = context;
			m_Context.Database.EnsureCreated();

			// Controller continuously creates new instances: Generate data only once (static .ctor)
	//		m_Context = new List< User > {
	//			new User() { Id = 1, Name = "Admin", Password = "12345" },
	//			new User() { Id = 2, Name = "John", Password = "Doe" },
	//			new User() { Id = 3, Name = "Alice", Password = "Cooper" } };
		}

		#endregion LifeCycle


		#region Operations

		#region Delete

		[HttpDelete( "{id}" )]
		public IActionResult Delete( long id )
		{
			var user = m_Context.Users.Where( u => u.Id == id ).FirstOrDefault();

			if ( user == null )
			{
				return NotFound();
			}

			m_Context.Remove( user );
			m_Context.SaveChanges();

			return NoContent();
		}

		#endregion Delete


		#region Get

//		public IActionResult Index()
//		{
//			return View();
//		}

//		[HttpGet]
//		public List< User > GetAll()
//		{
//			return m_Context;
//		}


		[HttpGet( "{name}", Name = "GetUser" )]
		public IActionResult GetByName( string name )
		{
			var user = m_Context.Users.Where( u => u.Username == name ).FirstOrDefault();

			if ( user == null )
			{
				return NotFound();
			}

			return new ObjectResult( user );
		}

		#endregion Get


		#region Post

		[HttpPost]
		public IActionResult Create( [FromBody] User user )
		{
			var allErrors = ModelState.Values.SelectMany(x => x.Errors);

			allErrors.ToList().ForEach( e => Console.WriteLine( e.ErrorMessage ) );

			user.Id		= m_Context.Users.Any() ? m_Context.Users.Max( u => u.Id ) + 1 : 1;
			user.Serial	= Guid.NewGuid().ToString();

			m_Context.Add( user );
			m_Context.SaveChanges();
			
			return CreatedAtRoute( "GetUser", new { name = user.Username }, user );
			//return CreatedAtAction( "GetUser", "user", new { name = user.Name }, user );
		}

		#endregion Post


		#region Put

		[HttpPut( "{id}" )]
		public IActionResult Update( long id, [FromBody] User user )
		{
			var currentUser = m_Context.Users.Where( u => u.Id == id ).FirstOrDefault();

			if ( currentUser == null )
			{
				return NotFound();	
			}

			currentUser.Username = user.Username;
			currentUser.Password = user.Password;

			m_Context.Update( currentUser );
			m_Context.SaveChanges();

			return NoContent();
		}

		#endregion Put

		#endregion Operations


		#region Members

		private readonly UserDbContext m_Context;

		#endregion Members
	}
}
