﻿const uri = 'api/user';

function createUser() {
	const userName = $('#add_name');
	const passwordElement = $('#add_password');

	const user = {
		'username': userName.val(),
		'password': passwordElement.val()
	};

	$.ajax({
		type: 'POST',
		url: uri,
		contentType: 'application/json',
		data: JSON.stringify(user),
		error: function (xhr, status, error) {
			alert('Error: ' + status);
		},
		success: function (result) {
			getData();

			userName.val('');
			passwordElement.val('');
		}
	});
}

$('.createUserForm').on('submit', function () {
	createUser();
});