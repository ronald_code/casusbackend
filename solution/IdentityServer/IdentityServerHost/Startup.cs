﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServerHost
{
	public class Startup
	{
		#region Operations

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc();

			services.AddDbContext< UserDbContext >( options =>
				options.UseSqlite( "DataSource=users.db;" ) ); // Create persistent (file) storage.

			// TODO: AddDeveloperSigningCredential creates temporary key material for signing tokens.
			// http://docs.identityserver.io/en/release/topics/crypto.html#refcrypto
			services.AddIdentityServer()
				.AddDeveloperSigningCredential()
				.AddInMemoryApiResources( Config.GetApiResources() )
				.AddInMemoryClients( Config.GetClients() );
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			// TODO: ? app.UseCookieAuthentication
			// TODO: ? app.UseOAuthAuthentication();
			app.UseDefaultFiles();
			app.UseStaticFiles();
			app.UseIdentityServer();
			app.UseMvc();
		}

		#endregion Operations
	}
}
