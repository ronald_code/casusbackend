﻿using System.Collections.Generic;
using IdentityServer4.Models;
using IdentityServer4.Test;
using Shared;

namespace IdentityServerHost
{
	public static class Config
	{
		#region Operations

		public static IEnumerable< ApiResource > GetApiResources()
		{
			return new List< ApiResource > 
			{ 
				new ApiResource( Constants.ApiName, Constants.ApiDescription )
			};
		}


		public static IEnumerable< Client > GetClients()
		{
			return new List< Client >
			{
				new Client
				{
					ClientId = Constants.ClientId,
					AccessTokenLifetime = Constants.AccessTokenLifetimeSeconds, // JWT expiration.
					AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
					ClientSecrets = { new Secret( Constants.ClientSecret.Sha256() ) }, // Used to sign access token.
					AllowedScopes = { Constants.ApiName }
				}
			};
		}


		public static List< TestUser > GetUsers()
		{
			return new List< TestUser >
			{
				new TestUser { SubjectId = "1", Username = "Alice", Password = "Cooper" },
				new TestUser { SubjectId = "2", Username = "Johnny", Password = "Cash" }
			};
		}

		#endregion Operations
	}
}
