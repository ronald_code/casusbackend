﻿using Microsoft.EntityFrameworkCore;

namespace IdentityServerHost
{
	public class UserDbContext : DbContext
	{
		#region LifeCycle

		public UserDbContext( DbContextOptions options ) : base( options )
		{
		}

		#endregion LifeCycle


		#region Operations
		// TODO:
		//		protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
		//		{
		//			optionsBuilder.UseSqlite( "DataSource=users.db;" ); // Create persistent (file) storage.
		//		}

		#endregion Operations


		#region Properties

		public DbSet< User > Users { get; set; }

		#endregion Properties
	}
}
