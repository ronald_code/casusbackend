﻿namespace IdentityServerHost
{
	public class User
	{
		#region Properties

		public long Id { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public string Serial { get; set; }

		#endregion Properties
	}
}
