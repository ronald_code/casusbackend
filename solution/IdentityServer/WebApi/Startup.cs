﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Shared;

namespace WebApi
{
	public class Startup
	{
		#region Operations

		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}


		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvcCore()
				.AddAuthorization()
				.AddJsonFormatters();

			services.AddAuthentication( "Bearer" )
				.AddIdentityServerAuthentication( options =>
				{
					options.Authority = Constants.IdentityServerUrl;
					options.RequireHttpsMetadata = false;
					options.ApiName = Constants.ApiName;
				} );
		}


		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseAuthentication();
			app.UseMvc();
		}

		#endregion Operations


		#region Properties
		
		public IConfiguration Configuration { get; }

		#endregion Properties
	}
}
